import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;


public class ProcessArff extends JPanel implements ActionListener{
	
	private static JTextField source_textfield;
	JButton btnChoose;
	JFileChooser chooser;
	String choosertitle;
	JTextPane outputResult;
	JScrollPane scrollPane;
	private JComboBox classifierCombobox;
	private static JFrame processArffFrame;
	/**
	 * Create the panel.
	 */
	public ProcessArff() {
		setLayout(null);
		JLabel lblChooseArffFile = new JLabel("Choose ARFF File");
		lblChooseArffFile.setBounds(23, 29, 108, 16);
		add(lblChooseArffFile);
		
		source_textfield = new JTextField();
		source_textfield.setBounds(143, 23, 298, 28);
		add(source_textfield);
		source_textfield.setColumns(10);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(353, 11, 0, 16);
		add(textPane);
		
		JButton sourceBtn = new JButton("Choose");
		sourceBtn.setBounds(453, 24, 91, 29);
		sourceBtn.addActionListener(this);
		add(sourceBtn);
		
		outputResult = new JTextPane();
		
		JButton btnProcess = new JButton("Process");
		btnProcess.setBounds(207, 382, 85, 29);
		btnProcess.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				passArfftoWeka(source_textfield.getText());
			}
		});
		add(btnProcess);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(304, 382, 85, 29);
		btnCancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				processArffFrame.setVisible(false);
				processArffFrame.dispose();
			}
		});
		add(btnCancel);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(23, 107, 950, 241);
		add(scrollPane);
		
		final JLabel lblClassifier = new JLabel("Classifier");
		lblClassifier.setBounds(23, 79, 108, 16);
		add(lblClassifier);
		
		classifierCombobox = new JComboBox();
		classifierCombobox.setModel(new DefaultComboBoxModel(new String[] {"Support Vector Machine", "Naive Bayes"}));
		classifierCombobox.setBounds(143, 68, 222, 27);
		add(classifierCombobox);
	}
	
	public void actionPerformed(ActionEvent e) {
	    int result;
	    chooser = new JFileChooser(); 
	    chooser.setCurrentDirectory(new java.io.File("."));
	    chooser.setDialogTitle(choosertitle);
	    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	    //
	    // disable the "All files" option.
	    //
	    chooser.setAcceptAllFileFilterUsed(false);
	    if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
	    	source_textfield.setText(chooser.getSelectedFile().toString());
	    }
	}
	
	public void passArfftoWeka (String filePath) {
		Process proc;
		try {
			System.out.println("Ran : "+filePath);
			int selectedClassifier = classifierCombobox.getSelectedIndex();
			switch (selectedClassifier) {
			case 0:
				proc = Runtime.getRuntime().exec("java -classpath $CLASSPATH:./jars/weka.jar:./jars/libsvm.jar weka.classifiers.functions.LibSVM -S 0 -K 0 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -seed 1 -no-cv -t "+filePath);
				break;
			case 1:
				proc = Runtime.getRuntime().exec("java -classpath $CLASSPATH:jars/weka.jar weka.classifiers.bayes.NaiveBayes -no-cv -t "+filePath);
				break;

			default:
				proc = Runtime.getRuntime().exec("java -classpath $CLASSPATH:jars/weka.jar:jars/libsvm.jar weka.classifiers.functions.LibSVM -S 0 -K 0 -D 3 -G 0.0 -R 0.0 -N 0.5 -M 40.0 -C 1.0 -E 0.001 -P 0.1 -seed 1 -no-cv -t "+filePath);
				break;
			}
			// Then retreive the process output
			InputStream in = proc.getInputStream();
			byte b[]=new byte[in.available()];
		    in.read(b,0,b.length);
		    outputResult.read(in,filePath);
		    scrollPane.setViewportView(outputResult);
			InputStream err = proc.getErrorStream();
			proc.destroy();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Dimension getPreferredSize(){
		return new Dimension(1000, 450);
    }
	
	public static void showDialog() {
		processArffFrame = new JFrame("");
		ProcessArff panel = new ProcessArff();
		source_textfield.setText(WekaFile.fileLocation);
		processArffFrame.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
//				System.exit(0);
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		processArffFrame.getContentPane().add(panel,"Center");
		processArffFrame.setSize(panel.getPreferredSize());
		processArffFrame.setVisible(true);
	}
}
