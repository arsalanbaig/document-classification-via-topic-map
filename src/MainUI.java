import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.text.NumberFormat;

import javax.swing.JCheckBox;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.text.NumberFormatter;
import javax.swing.JFormattedTextField;

public class MainUI extends JPanel implements ActionListener {

	/**
	 * Create the panel.
	 */
	JButton source_button;

	JFileChooser chooser;
	String choosertitle;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField source_textfield;
	public static JTextField dest_textfield;
	private JCheckBox chckbxRemoveStopWords;
	private JCheckBox chckbxUseOnlyDistinct;
	private JCheckBox chckbxWordCntFrq;
	private JFormattedTextField wordSizeLimit;
	public static JFrame mainFrame;

	public MainUI() {
		source_button = new JButton("Choose");
		source_button.setBounds(437, 27, 75, 29);
		source_button.addActionListener(this);
		setLayout(null);
		add(source_button);

		JLabel lblSourceFolder = new JLabel("Source Folder");
		lblSourceFolder.setBounds(6, 32, 91, 16);
		add(lblSourceFolder);

		JLabel lblDestination = new JLabel("Destination");
		lblDestination.setBounds(6, 94, 91, 16);
		add(lblDestination);

		JButton destination_button = new JButton("Browse");
		destination_button.setBounds(437, 81, 75, 29);
		destination_button.addActionListener(this);
		add(destination_button);

		source_textfield = new JTextField();
		source_textfield.setBounds(114, 26, 301, 28);
		add(source_textfield);
		source_textfield.setColumns(10);

		dest_textfield = new JTextField();
		dest_textfield.setBounds(114, 88, 301, 28);
		add(dest_textfield);
		dest_textfield.setColumns(10);
		
		chckbxRemoveStopWords = new JCheckBox("Remove Stop Words");
		chckbxRemoveStopWords.setBounds(66, 141, 156, 23);
		add(chckbxRemoveStopWords);

		chckbxUseOnlyDistinct = new JCheckBox("Use only distinct words");
		chckbxUseOnlyDistinct.setBounds(66, 176, 186, 23);
		add(chckbxUseOnlyDistinct);

		NumberFormat format = NumberFormat.getIntegerInstance();
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setAllowsInvalid(false);
		formatter.setMinimum(0);
		formatter.setMaximum(15);
		// If you want the value to be committed on each keystroke instead of
		// focus lost
		formatter.setCommitsOnValidEdit(true);
		wordSizeLimit = new JFormattedTextField(formatter);
		wordSizeLimit.setBounds(338, 209, 77, 28);
		wordSizeLimit.setVisible(false);
		add(wordSizeLimit);

		chckbxWordCntFrq = new JCheckBox("Word count Frequency");
		chckbxWordCntFrq.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if (chckbxWordCntFrq.isSelected()) {
					wordSizeLimit.setVisible(true);
				} else {
					wordSizeLimit.setVisible(false);
				}
			}
		});
		chckbxWordCntFrq.setBounds(66, 211, 196, 23);
		add(chckbxWordCntFrq);

		JLabel lblClassificationMethod = new JLabel("Classification Method");
		lblClassificationMethod.setBounds(6, 263, 146, 29);
		add(lblClassificationMethod);

		JLabel lblCombination = new JLabel("Combinations");
		lblCombination.setBounds(6, 304, 146, 16);
		add(lblCombination);

		JComboBox<String> serviceCombination = new JComboBox<>();
		serviceCombination.setModel(new DefaultComboBoxModel(new String[] {"JCalais-with-TextAnalytics", "Aylien-with-TextAnalytics", "TextAnalytics-with-Aylien", "TextAnalytics-with-JCalais", "TextAnalytics-and-JCalais", "TextAnalytics-and-Aylien", "Aylien-with-JCalais", "JCalais-with-Aylien", "JCalais-and-Aylien", "TextAnaylitics", "JCalais", "Aylien", "JCalaisUAylienUTextAnalytics", "JCalais|Aylien|TextAnalytics (Intersection)"}));
		serviceCombination.setBounds(175, 304, 253, 27);
		add(serviceCombination);

		JComboBox<String> methodSelection = new JComboBox<>();
		methodSelection.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (methodSelection.getSelectedItem() == "BOW") {
					serviceCombination.setVisible(false);
					lblCombination.setVisible(false);
				} else {
					serviceCombination.setVisible(true);
					lblCombination.setVisible(true);
				}
			}
		});
		methodSelection.setModel(new DefaultComboBoxModel<String>(new String[] {
				"Topic Map", "BOW" }));
		methodSelection.setBounds(175, 265, 138, 27);
		add(methodSelection);

		JButton btnGenerateArff = new JButton("Generate ARFF");
		btnGenerateArff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println(serviceCombination.getSelectedItem()
						.toString());
				int wordFrequency = (chckbxWordCntFrq.isSelected()) ? (int)wordSizeLimit.getValue() : Constants.MIN_WORD_COUNT_FREQUENCY;
				if (methodSelection.getSelectedItem() == "Topic Map") {
					System.out.println("Topic Map Started"
							+ source_textfield.getText() + " : "
							+ serviceCombination.getSelectedIndex());
					new ExtractTopics(source_textfield.getText(),
							serviceCombination.getSelectedIndex(), filterSwitch(), wordFrequency).start();
				} else if (methodSelection.getSelectedItem() == "BOW") {
					System.out.println("Bag of Words");
					new BOW(source_textfield.getText(), filterSwitch(), wordFrequency).start();
				}
			}
		});
		btnGenerateArff.setBounds(115, 366, 117, 29);
		add(btnGenerateArff);

		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(374, 366, 117, 29);
		btnCancel.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});
		add(btnCancel);
		
		final JButton btnProcessArff = new JButton("Process ARFF");
		btnProcessArff.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ProcessArff.showDialog();
			}
		});
		btnProcessArff.setBounds(245, 366, 117, 29);
		add(btnProcessArff);

	}
	
	private int filterSwitch () {
		Boolean removeStopwordsChkd = chckbxRemoveStopWords.isSelected();
		Boolean onlyDistinctwordsChkd = chckbxUseOnlyDistinct.isSelected();
		if((removeStopwordsChkd && onlyDistinctwordsChkd) || onlyDistinctwordsChkd) {
			return Constants.USE_ONLY_DISTINCT_WORDS;
		} else if (removeStopwordsChkd) {
			return Constants.REMOVE_ONLY_STOP_WORDS;
		}
		return Constants.DONT_FILTER;
	}

	public void actionPerformed(ActionEvent e) {
		int result;
		chooser = new JFileChooser();
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle(choosertitle);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		//
		// disable the "All files" option.
		//
		chooser.setAcceptAllFileFilterUsed(false);
		//
		if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			if (e.getActionCommand() == "Choose")
				source_textfield.setText(chooser.getSelectedFile().toString());
			else
				dest_textfield.setText(chooser.getSelectedFile().toString());
		}
	}

	public Dimension getPreferredSize() {
		return new Dimension(600, 500);
	}

	public static void main(String s[]) {
		JFrame mainFrame = new JFrame("");
		MainUI panel = new MainUI();
		mainFrame.addWindowListener(new WindowListener() {

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub

			}
		});

		mainFrame.getContentPane().add(panel, "Center");
		mainFrame.setSize(panel.getPreferredSize());
		mainFrame.setVisible(true);
	}
}
