import java.io.Serializable;
import java.util.List;


@SuppressWarnings("serial")
public class DataContainer implements Serializable {
	public List<Document> documents;
	public Topics topics;
}
