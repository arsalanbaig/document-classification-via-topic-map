/**
 * Text Classification 1.1 starting client for Java.
 *
 * In order to run this example, the license key obtained for the Text Classification API
 * must be included in the key variable. If you don't know your key, check your personal
 * area at Textalytics (https://textalytics.com/personal_area)
 *
 * Once you have the key, edit the parameters and call "javac *.java; java ClassClient"
 *
 * You can find more information at http://textalytics.com/core/class-1.1-info
 *
 * @author     Textalytics
 * @contact    http://www.textalytics.com (http://www.daedalus.es)
 * @copyright  Copyright (c) 2014, DAEDALUS S.A. All rights reserved.
 */
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.*;


/**
 * This class implements a starting client for Text Classification  
 */
public class TextAnalytics {

	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		// We define the variables needed to call the API
		String api = "http://textalytics.com/core/class-1.1";
		String key = "996426a13b997b78baa519efdaed61be";//Constants.TextAlyticsAppKey;//"3037735f89f969788e0bf90055e398f3";
		String txt = "Path: cantaloupe.srv.cs.cmu.edu!das-news.harvard.edu!ogicse!uwm.edu!cs.utexas.edu!uunet!noc.near.net!bigboote.WPI.EDU!ching "
				+ "From: ching@coyote.WPI.EDU (Jay Heminger) "
				+ "Newsgroups: rec.sport.hockey"
				+ "Subject: Re: NCAA finals...Winner????"
				+ "Message-ID: <1pqgt9$r46@bigboote.WPI.EDU>"
				+ "Date: 5 Apr 93 23:55:21 GMT"
				+ "Article-I.D.: bigboote.1pqgt9$r46 References: <1993Apr4.165655.16932@miavx1.acs.muohio.edu> Distribution: world Organization: Worcester Polytechnic Institute Lines: 9 NNTP-Posting-Host: coyote.wpi.edu Originator: ching@coyote.WPI.EDUMaine beat LSSU 5-4. --  ------------------------THE LOGISTICIAN REIGNS SUPREME!!!----------------------|									      | "
				+ "|   GO BLUE!!!   GO TIGERS!!!   GO PISTONS!!!   GO LIONS!!!   GO RED WINGS!!! | -------------------------------ching@wpi.wpi.edu-------------------------------";
		String model = "IPTC_es";  // IPTC_es/IPTC_en/IPTC_fr/IPTC_it/IPTC_ca/EUROVOC_es_ca/BusinessRep_es/BusinessRepShort_es

		Post post = new Post (api);
		post.addParameter("key", key);
		post.addParameter("txt", txt);
		post.addParameter("model", model);
		post.addParameter("of", "xml");
		String response = post.getResponse();

		// Show response
		System.out.println("Response");
		System.out.println("============");
		System.out.println(response);

		// Prints the specific fields in the response (categories)
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(new ByteArrayInputStream(response.getBytes()));
		doc.getDocumentElement().normalize();
		Element response_node = doc.getDocumentElement();
		System.out.println("\nCategories:");
		System.out.println("==============");
		try {
			NodeList status_list = response_node.getElementsByTagName("status");
			Node status = status_list.item(0);
			NamedNodeMap attributes = status.getAttributes();
			Node code = attributes.item(0);
			if(!code.getTextContent().equals("0")) {
				System.out.println("Not found");
			} else {
				NodeList category_list = response_node.getElementsByTagName("category_list");
				if(category_list.getLength()>0){
					Node categories = category_list.item(0);          
					NodeList category = categories.getChildNodes();
					String output = "";
					for(int i=0; i<category.getLength(); i++) {
						Node info_category = category.item(i);  
						NodeList child_category = info_category.getChildNodes();
						String label = "";
						String code_cat = "";
						String relevance = "";
						for(int j=0; j<child_category.getLength(); j++){
							Node n = child_category.item(j);
							String name = n.getNodeName();
							if(name.equals("code"))
								code_cat = n.getTextContent();
							else if(name.equals("label"))
								label = n.getTextContent();
							else if(name.equals("relevance"))
								relevance = n.getTextContent();
						}
						output += " + " + label + " (" +  code_cat + ")\n";
						output += "   -> relevance: " + relevance + "\n";
					}
					if(output.isEmpty())
						System.out.println("Not found");
					else
						System.out.print(output);
				}
			}
		} catch (Exception e) {
			System.out.println("Not found");
		}
	}

	public static List<Topic> getTopics (String txt) throws IOException, ParserConfigurationException, SAXException {
		List<Topic> topics = new ArrayList<Topic>();
//		Path uri = Paths.get(path);
//		String txt = StopWords.filterDistinctWords(path);
//		try {
//			for(String line: Files.readAllLines(uri, Charset.defaultCharset())){
//				txt +=line+"\n";
//			}
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		String api = "http://textalytics.com/core/class-1.1";
		String api = "http://api.meaningcloud.com/class-1.1";
		String key = Constants.TextAlyticsAppKey;//"3037735f89f969788e0bf90055e398f3";
		String model = "IPTC_en";  // IPTC_es/IPTC_en/IPTC_fr/IPTC_it/IPTC_ca/EUROVOC_es_ca/BusinessRep_es/BusinessRepShort_es

		Post post = new Post (api);
		post.addParameter("key", key);
		post.addParameter("txt", txt);
		post.addParameter("model", model);
		post.addParameter("of", "xml");
		String response = post.getResponse();

		// Show response
//		System.out.println("Response");
//		System.out.println("============");
//		System.out.println(response);

		// Prints the specific fields in the response (categories)
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(new ByteArrayInputStream(response.getBytes()));
		doc.getDocumentElement().normalize();
		Element response_node = doc.getDocumentElement();
//		System.out.println("\nCategories:");
//		System.out.println("==============");
		try {
			NodeList status_list = response_node.getElementsByTagName("status");
			Node status = status_list.item(0);
			NamedNodeMap attributes = status.getAttributes();
			Node code = attributes.item(0);
			if(!code.getTextContent().equals("0")) {
				System.out.println("Not found");
			} else {
				NodeList category_list = response_node.getElementsByTagName("category_list");
				if(category_list.getLength()>0){
					Node categories = category_list.item(0);          
					NodeList category = categories.getChildNodes();
					String output = "";
					for(int i=0; i<category.getLength(); i++) {
						Node info_category = category.item(i);  
						NodeList child_category = info_category.getChildNodes();
						String label = "";
						String code_cat = "";
						String relevance = "";
						Topic topic = new Topic();
						for(int j=0; j<child_category.getLength(); j++){
							Node n = child_category.item(j);
							String name = n.getNodeName();
							if(name.equals("code"))
								code_cat = n.getTextContent();
							else if(name.equals("label")){
								label = n.getTextContent();
								topic.topicName = label;
								if(!Topics.topicsDict.containsKey(topic.topicName)){
					        		Topics.topicsDict.put(topic.topicName, Topics.currentPosition());
					        	}
							}
							else if(name.equals("relevance")){
								relevance = n.getTextContent();
								topic.probability = Double.valueOf(relevance)/100;
								if(!topic.topicName.isEmpty())
									topics.add(topic);
//								topic = null;
							}
						}
//						output += " + " + label + " (" +  code_cat + ")\n";
//						output += "   -> relevance: " + relevance + "\n";
					}
//					if(output.isEmpty())
//						System.out.println("Not found");
//					else
//						System.out.print(output);
				}
			}
		} catch (Exception e) {
			System.out.println("Not found");
		}
		return topics;
	}
}
