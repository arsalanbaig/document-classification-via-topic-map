import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@SuppressWarnings("serial")
public class Topics implements Serializable {
	public static int counter = 0;
	public static LinkedHashMap topicsDict = new LinkedHashMap();
	public static List<String> classes = new ArrayList<String>();
	private static Topics topics = new Topics();
	private Topics() {
		
	}
	
	public static Topics getInstance () {
		return topics;
	}
	
	@SuppressWarnings("static-access")
	public static void setInstance(Topics recoveredTopics){
		topics = recoveredTopics;
		topicsDict = recoveredTopics.topicsDict;
		counter = recoveredTopics.counter;
	}
	
	public static void reset () {
		counter = 0;
		topicsDict.clear();
		classes.clear();
	}
	
	public static Integer currentPosition() {
		counter++;
		return counter;
	}
	
	public static int totalTopics() {
		return topicsDict.size();
	}
	
	@SuppressWarnings("rawtypes")
	public static void generateTopicMatrixRow(List<Document> documents) {
		for(Document doc : documents){
			doc.topicMatrixRow.clear();
			Iterator it = topicsDict.entrySet().iterator();
			Boolean lock = false;
			while(it.hasNext()){
				Map.Entry pair = (Map.Entry)it.next();
				lock = false;
				for(Topic topic : doc.topics){
					if(topic.topicName.equals(pair.getKey())){
						doc.topicMatrixRow.add(1);
						lock = true;
						break;
					}
				}
				if(!lock){
					doc.topicMatrixRow.add(0);
				}
			}
		}
	}

}
