import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class Tag {
	public String tagName;
	public float relevance;
	public String type;
	
	public void setTagName(String name){
		String[] chunks = name.split("/");
		try {
			this.tagName = URLDecoder.decode(chunks[chunks.length - 1], "UTF-8");
			this.type = URLDecoder.decode(chunks[chunks.length - 2], "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setTagRelevance(String relevance){
		String[] relChunks = relevance.split("/");
		this.relevance = Float.valueOf(relChunks[relChunks.length - 1]);
	}
}
