import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class WekaFile {
	public static Loader loader;
	public static String fileLocation = "";
	public static Boolean isWriting = false;
	@SuppressWarnings("static-access")
	public static Boolean generateTopicFile(String relationName, List<Document> documents) {
		isWriting = true;
		String arffFile ="% Starting of ARFF file for MS-Project 1 - DATA CLASSIFICATION VIA TOPIC MAP MATRIX \n";
		
		String relation = "@relation "+relationName+"\n";
		List<String> attributes = new ArrayList<String>();
		Iterator it = Topics.getInstance().topicsDict.entrySet().iterator();

		// ADDING TOPICS ID TO ATTRIBUTE 
		while(it.hasNext()){
			Map.Entry pair = (Map.Entry)it.next();
			String attribute = "@attribute '"+pair.getValue()+"' numeric\n";
			attributes.add(attribute);
		}
		
		//ADDING CLASS NAME TO ATTRIBUTE
		String attribute = "@attribute 'class' {";//{C1,C2,C3,C4,C5}\n";
		for(String clazz : Topics.classes){
			attribute += clazz+",";
		}
		attribute = attribute.substring(0, attribute.length()-1);
		attribute += "}\n";
		attributes.add(attribute);
		
		// ADDING DATA
		String data = "@data\n";
		for(Document doc : documents) {
			for(int topicID : doc.topicMatrixRow){
				data += topicID+",";
			}
			data += doc.clazz+"\n";
		}
		
		// COMBINING ALL DATA TO WEKAFILE
		
		arffFile += relation;
		for(String attr : attributes){
			arffFile += attr;
		}
		arffFile += data;
//		System.out.println(arffFile);
		writeToFile(relationName, arffFile);
		return true;
	}
	
	public static Boolean generateWordFile(String relationName, List<Document> documents) throws IOException {
		isWriting = true;
		String customLocation = MainUI.dest_textfield.getText();
		System.out.println(customLocation.equals(""));
		File folder = new File((!customLocation.equals("")) ? customLocation+"/arffFiles" : "arffFiles");
		if(!folder.exists()){
			folder.mkdir();
		}
		File tempFile = new File((!customLocation.equals("")) ? customLocation+"/arffFiles/BowClassification.arff" : "arffFiles/BowClassification.arff");
		if(tempFile.exists()){
			tempFile.delete();
		}
		FileWriter fileWriter = new FileWriter((!customLocation.equals("")) ? customLocation+"/arffFiles/BowClassification.arff" : "arffFiles/BowClassification.arff", true);
		
		String arffFile ="% Starting of ARFF file for MS-Project 1 - DATA CLASSIFICATION VIA TOPIC MAP MATRIX \n";
		appendToFile(arffFile, fileWriter);
		
		String relation = "@relation "+relationName+"\n";
		appendToFile(relation, fileWriter);
		
		Iterator it = StopWords.getInstance().totalFilteredBOW.entrySet().iterator();

		// ADDING TOPICS ID TO ATTRIBUTE 
		while(it.hasNext()){
			Map.Entry pair = (Map.Entry)it.next();
			String attribute = "@attribute '"+pair.getValue()+"' numeric\n";
			appendToFile(attribute, fileWriter);
		}
		
		//ADDING CLASS NAME TO ATTRIBUTE
		String attribute = "@attribute 'class' {";//{C1,C2,C3,C4,C5}\n";
		for(String clazz : Topics.classes){
			attribute += clazz+",";
		}
		attribute = attribute.substring(0, attribute.length()-1);
		attribute += "}\n";
		appendToFile(attribute, fileWriter);
		
		// ADDING DATA
		String data = "@data\n";
		appendToFile(data, fileWriter);
		for(Document doc : documents) {
			for(int topicID : doc.bowMatrixRow){
				appendToFile(topicID+",", fileWriter);
			}
			appendToFile(doc.clazz+"\n", fileWriter);
		}
		fileWriter.close();
		fileLocation = "arffFiles/BowClassification.arff";
		isWriting = false;
	    System.out.println();
	    System.out.println("Data Successfully Written.");
		
		return true;
	}
	
	private static void appendToFile(String data, FileWriter file) {
		try {
			file.write(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void writeToFile(String fileName, String data){
		String customLocation = MainUI.dest_textfield.getText();
		File folder = new File((!customLocation.equals("")) ? customLocation+"/arffFiles" : "arffFiles");
		if(!folder.exists()){
			folder.mkdir();
		}
		try (PrintStream out = new PrintStream(new FileOutputStream((!customLocation.equals("")) ? customLocation+"/arffFiles/"+fileName+".arff" : "arffFiles/"+fileName+".arff"))) {
		    out.print(data);
		    out.close();
		    fileLocation = (!customLocation.equals("")) ? customLocation+"/arffFiles/"+fileName+".arff" : "arffFiles/"+fileName+".arff";
		    System.out.println();
		    System.out.println("Data Successfully Written.");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.println();
			System.out.println("Data Writing Failed.");
		}
		isWriting = false;
	}
	public static void writeTopicsToFile(String method, String clazz, String fileName, String data){
		File folder = new File(method);
		if(!folder.exists()){
			folder.mkdir();
		}
		File newFolder = new File (method+"/"+clazz);
		if(!newFolder.exists()) {
			newFolder.mkdir();
		}
		try (PrintStream out = new PrintStream(new FileOutputStream(method+"/"+clazz+"/"+fileName+".txt"))) {
			out.print(data);
			out.close();
			System.out.println();
			System.out.println("Data Successfully Written.");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println();
			System.out.println("Data Writing Failed.");
		}
	}
}
