import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class DeserializeObjects {

	@SuppressWarnings({ "unchecked", "resource" })
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		InputStream file;
		List<Document> documents = new ArrayList<Document>();
		DataContainer data = new DataContainer();
		try {
			file = new FileInputStream("Group1.dat");
			InputStream buffer = new BufferedInputStream(file);

			ObjectInput input = new ObjectInputStream (buffer);
			{
				data = (DataContainer)input.readObject();
				documents = data.documents;
				Topics.setInstance(data.topics);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(Document doc : documents){
			for(Topic topic : doc.topics){
				if(!Topics.topicsDict.containsKey(topic.topicName)){
					Topics.topicsDict.put(topic.topicName, Topics.currentPosition());
				}
			}
			System.out.println("Filename : "+doc.name);
			System.out.println("========= Matrix Row =========");
//			System.out.println(doc.matrixRow);
			System.out.println("========= Topics =========");
			for(Topic s : doc.topics){
				System.out.print("[ID = "+doc.topicByNum.get(s.topicName)+"]");
				System.out.println(" [Topic = "+s.topicName+"] [P = "+s.probability+"]");
			}
			System.out.println();
			System.out.println("========= Tags =========");
			for(Tag s : doc.tags){
				System.out.println("[Name = "+s.tagName+"]");
				System.out.println("[Type = "+s.type+"]");
				System.out.println("[Relevance = "+s.relevance+"]");
			}
			System.out.println();
			
				
			}
		Topics.generateTopicMatrixRow(documents);
		System.out.println("========= Topic Matrix =========");
		System.out.println();
		System.out.print("\t\t");				
		for(int i=1;i<=Topics.totalTopics();i++){
			System.out.print(i+"\t");				
		}
		System.out.println();
		System.out.println();
		for(Document doc : documents){
//			char[] bits = doc.matrixRow.toCharArray();
			
			System.out.print(doc.name+"\t\t");
			for(Integer c : doc.topicMatrixRow){
				System.out.print(c+"\t");				
			}
			System.out.print(doc.clazz+"\t");
			System.out.println();
			
		}
		
		WekaFile.generateTopicFile("TopicMap", documents);
		
	}

}
