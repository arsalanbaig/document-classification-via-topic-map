import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class BOW extends Thread {
	private File folder;
	private static List<Document> documents = new ArrayList<Document>();
	private int filterSwitch;
	private int wordFrequency;
	
	public BOW (String folderPath, int filterSwitch, int wordFrequency) {
		folder = new File(folderPath);
		this.filterSwitch = filterSwitch;
		this.wordFrequency = wordFrequency;
	}
	
	public void bowClassification() {
		// TODO Auto-generated method stub
		try {
			File[] folders = folder.listFiles();
			for(File folder : folders){
//				System.out.println(folder.getAbsolutePath());
				if(folder.isDirectory()){
					File[] listoffiles = folder.listFiles();
					for(File file : listoffiles) {
						if(file.isFile()  && !file.getName().contains(".DS_Store")){
							Document document = new Document();
							document.name = file.getName();
							document.clazz = folder.getName();
							if(!Topics.classes.contains(document.clazz)){
								Topics.classes.add(document.clazz);
							}
							document.filteredDoc = StopWords.filterDistinctWords(file.getAbsolutePath(), this.wordFrequency);
							document.words = document.filteredDoc.docDistinctWordList;
							document.fillBowDict();
							documents.add(document);
						}
					}
				}
			}
			FilteredResults.showDialog(documents);
			StopWords.generateMatrixBOW(documents);
			WekaFile.generateWordFile("BOWClassification", documents);
		} catch (Exception e) {
			System.out.println(e.toString());
			// TODO: handle exception
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		bowClassification();
	}
}
