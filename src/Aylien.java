import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


public class Aylien {
	public static List<Topic> sendRequestToAylien(String text) {
		String appKey = Constants.AylienAppKey;
		String appID = Constants.AylienAppID;
//		Path uri = Paths.get(path);
//		String text = StopWords.filterDistinctWords(path);
	    URL url;
	    HttpURLConnection connection = null;
	    try {
//	    	for(String line: Files.readAllLines(uri, Charset.defaultCharset())){
//	    		text +=line+"\n";
//	    	}
	      //Create connection
	      String urlParameters = "text="+URLEncoder.encode(text,"UTF-8");
	      url = new URL("http://api.aylien.com/api/v1/classify");
	      connection = (HttpURLConnection)url.openConnection();
	      connection.setRequestMethod("GET");
	      connection.setRequestProperty("Content-Type", 
	           "application/x-www-form-urlencoded");
	      
	      connection.setRequestProperty("Content-Length", "" + 
	               Integer.toString(urlParameters.getBytes().length));
	      connection.setRequestProperty("Content-Language", "en-US"); 
	      connection.setRequestProperty("X-AYLIEN-TextAPI-Application-Key", appKey); 
	      connection.setRequestProperty("X-AYLIEN-TextAPI-Application-ID", appID); 

	      System.out.println(connection.getRequestProperties());
	      connection.setUseCaches (false);
	      connection.setDoInput(true);
	      connection.setDoOutput(true);
	      //Send request
	      DataOutputStream wr = new DataOutputStream (
	                  connection.getOutputStream ());
	      
	      wr.writeBytes (urlParameters);
	      wr.flush ();
	      wr.close ();

	      //Get Response    
	      InputStream is = connection.getInputStream();
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is));
	      String line;
	      StringBuffer response = new StringBuffer(); 
	      while((line = rd.readLine()) != null) {
	        response.append(line);
	        response.append('\r');
	      }
	      rd.close();
//	      return response.toString();
	      return parseJSONtoObj(response.toString());

	    } catch (Exception e) {

	      e.printStackTrace();
	      return parseJSONtoObj("");

	    } finally {

	      if(connection != null) {
	        connection.disconnect(); 
	      }
	    }
	}
	
	public static List<Topic> parseJSONtoObj (String jsonString) {
		JSONParser jsonParser = new JSONParser();
		List<Topic> topics = new ArrayList<Topic>();
		try {
			JSONObject mainObject = (JSONObject) jsonParser.parse(jsonString);
			JSONArray categories = (JSONArray) mainObject.get("categories");
			for(int i=0; i<categories.size(); i++){
				JSONObject category = (JSONObject) categories.get(i);
				Topic topic = new Topic();
				topic.topicName = (String) category.get("label");
				topic.probability = (double) category.get("confidence");
				topics.add(topic);
				if(!Topics.topicsDict.containsKey(topic.topicName)){
	        		Topics.topicsDict.put(topic.topicName, Topics.currentPosition());
	        	}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return topics;
	}
}
