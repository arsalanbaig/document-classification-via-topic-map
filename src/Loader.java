
public class Loader implements Runnable {
	private Thread thread;
	private String title;
	private Boolean isRunnable = true;
	private int count = 0;
	
	public Loader (String titleName) {
		title = titleName;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println(title);
		while(isRunnable){
			try {
				if(count > 25){
					System.out.println();
					System.out.print(". ");
					count = 0;
				}
				System.out.print(". ");
				count++;
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void start() {
		if(thread == null){
			thread = new Thread(this,title);
			thread.start();
		}
	}
	
	public void kill() {
		isRunnable = false;
	}
}
