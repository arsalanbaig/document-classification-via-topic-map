import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import mx.bigdata.jcalais.CalaisClient;
import mx.bigdata.jcalais.CalaisObject;
import mx.bigdata.jcalais.CalaisResponse;
import mx.bigdata.jcalais.rest.CalaisRestClient;


public class JCalais {
	private static CalaisClient client;

	public static List<Tag> retreiveTags (String path) {
		List<Tag> tags = new ArrayList<Tag>();
		client = new CalaisRestClient(Constants.jCalaisApiKey);
		try {
//			FileReader read = new FileReader(file.getAbsolutePath());
			CalaisResponse response = client.analyze(new FileReader(path));
			System.out.println(response.getTopics().toString());
			for (CalaisObject entity : response.getEntities()) {
//				System.out.println(entity.getField("_type") + ":" 
//						+ entity.getField("name"));
				Tag tag = new Tag();
				tag.type = entity.getField("_type");
				tag.tagName = entity.getField("name");
				tag.relevance = Float.parseFloat(entity.getField("relevance"));
				tags.add(tag);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tags;
	}
	
	public static List<Topic> retrieveTopics (String text) {
		List<Topic> topics = new ArrayList<Topic>();
		client = new CalaisRestClient(Constants.jCalaisApiKey);
//		String text = StopWords.filterDistinctWords(path);
		try {
//			CalaisResponse response = client.analyze(new FileReader(path));
			CalaisResponse response = client.analyze(text);
//			System.out.println(response.getTopics().toString());
			for (CalaisObject category : response.getTopics()) {
				System.out.println(category.getField("categoryName") + ":" 
						+ category.getField("score"));
				Topic topic = new Topic();
				topic.topicName = category.getField("categoryName");
				topic.probability = Double.valueOf(category.getField("score"));
				topics.add(topic);
				if(!Topics.topicsDict.containsKey(topic.topicName)){
	        		Topics.topicsDict.put(topic.topicName, Topics.currentPosition());
	        	}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return topics;
	}

}
