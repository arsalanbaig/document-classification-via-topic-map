import java.io.Serializable;


public class Topic implements Serializable {
	public String topicName;
	public double probability;
	
	public Topic() {}
	
	public Topic(String name, double prob) {
		this.topicName = name;
		this.probability = prob;
	}

}
