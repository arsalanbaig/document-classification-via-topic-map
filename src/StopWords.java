import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class StopWords {
	private static List<String> stopWordsList;
	public static LinkedHashMap totalFilteredBOW = new LinkedHashMap();
	public static LinkedHashMap totalDistinctBOW = new LinkedHashMap();
	private static int counter = 0;
	private static int disticntCounter = 0;
	private static StopWords stopWords = new StopWords();
	public static int totalWords = 0;
	public static int totalRawWords = 0;
	private StopWords() {
		createStopWordList();
	}
	
	public static StopWords getInstance() {
		return stopWords;
	}

	public static Integer currentPosition(Boolean distinct) {
		if(distinct) 
			disticntCounter ++ ;
		else
			counter++;
		return (distinct) ? disticntCounter : counter;
	}
	
	public static void createStopWordList () {
		stopWordsList = new ArrayList<String>();
		String stopWordsPath = "stop-words.txt";
		Path uri = Paths.get(stopWordsPath);
		String txt = "";
		try {
			for(String line: Files.readAllLines(uri, Charset.defaultCharset())){
//				System.out.println(line);
				stopWordsList.add(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static FilteredDocument filterStopWords(String filePath) {
		FilteredDocument filteredDoc = new FilteredDocument(); 
		Path uri = Paths.get(filePath);
		try {
			for(String line: Files.readAllLines(uri, Charset.forName("ISO-8859-1"))){
				if(line.length() <= 0){
					continue;
				}
				List<String> words = Arrays.asList(line.split(" "));
				for(String word : words){
					word = word.replaceAll("\\W", "");
					if(word == "" || word == "\\s")
						continue;
					if(!stopWordsList.contains(word) && word.length() > 2){
						filteredDoc.docStopWordList.add(word);
						filteredDoc.docStopWords += word+" ";
						if(!StopWords.totalFilteredBOW.containsKey(word)){
			        		StopWords.totalFilteredBOW.put(word, StopWords.currentPosition(false));
			        	}
					}
					filteredDoc.originalTrimDoc += word+" ";
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return filteredDoc;
	}
	
	public static FilteredDocument filterDistinctWords (String path, int minWordFreq) {
		minWordFreq = (minWordFreq <= 0) ? Constants.MIN_WORD_COUNT_FREQUENCY : minWordFreq;
		FilteredDocument filteredDoc = filterStopWords(path);
		System.out.println("Filtered Stop Words : Size : "+filteredDoc.docStopWordList.size()+" : "+filteredDoc.docStopWords);
		for(String datum : filteredDoc.docStopWordList){
			if(!filteredDoc.docDistinctWordList.contains(datum)){
				if(datum.length() > minWordFreq && datum.length() < Constants.MAX_WORD_COUNT_FREQUENCY){
					filteredDoc.docDistinctWordList.add(datum);
					filteredDoc.docDictinctWords += datum + " ";
					if(!StopWords.totalDistinctBOW.containsKey(datum)){
		        		StopWords.totalDistinctBOW.put(datum, StopWords.currentPosition(true));
		        	}
				}
			}
			totalRawWords++;
		}
		totalWords += filteredDoc.docDistinctWordList.size();
		System.out.println("Filter Distinct Words : Size : "+filteredDoc.docDistinctWordList.size()+" : "+filteredDoc.docDictinctWords);
		return filteredDoc;
	}
	
	public static void generateMatrixBOW(List<Document> documents) {
		for(Document doc : documents){
			doc.bowMatrixRow.clear();
			Iterator it = totalFilteredBOW.entrySet().iterator();
			Boolean lock = false;
			while(it.hasNext()){
				Map.Entry pair = (Map.Entry)it.next();
				lock = false;
				for(String word : doc.words){
					if(word.equals(pair.getKey())){
						doc.bowMatrixRow.add(1);
						lock = true;
						break;
					}
				}
				if(!lock){
					doc.bowMatrixRow.add(0);
				}
			}
		}
	}
}
