import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JList;
import javax.swing.JTextPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class FilteredResults extends JPanel {
	private JList documentList;
	private JScrollPane filteredResults;
	private JComboBox filterLevelBox;
	private JTextPane hiddenTextPane;
	public static JFrame resultsFrame;
	@SuppressWarnings("rawtypes")
	private DefaultListModel documentListModel = new DefaultListModel();

	/**
	 * Create the panel.
	 */
	@SuppressWarnings("unchecked")
	public FilteredResults(List<Document> documents) {
		setLayout(null);
		
		JLabel lblFilteredResults = new JLabel("Filtered Results");
		lblFilteredResults.setFont(new Font("Lucida Grande", Font.PLAIN, 25));
		lblFilteredResults.setHorizontalAlignment(SwingConstants.CENTER);
		lblFilteredResults.setBounds(199, 6, 224, 45);
		add(lblFilteredResults);
		
		JLabel lblNewLabel = new JLabel("Filter Level");
		lblNewLabel.setBounds(18, 67, 78, 16);
		add(lblNewLabel);
		
		filterLevelBox = new JComboBox();
		filterLevelBox.setModel(new DefaultComboBoxModel(new String[] {"Original Doc", "Stop words", "Distinct words", "Extracted Topics"}));
		filterLevelBox.setBounds(199, 63, 293, 27);
		add(filterLevelBox);
		
		JLabel lblDocuments = new JLabel("Documents");
		lblDocuments.setBounds(18, 117, 78, 16);
		add(lblDocuments);
		
		filteredResults = new JScrollPane();
		filteredResults.setBounds(199, 145, 773, 265);
//		filteredResults.set
		add(filteredResults);
		
		hiddenTextPane = new JTextPane();
		documentList = new JList(populateDocList(documents));
		documentList.setBounds(18, 145, 144, 265);
		documentList.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				if(!e.getValueIsAdjusting()){
					System.out.println(documentList.getSelectedIndex()+" : "+documentList.getSelectedValue());
					Document doc = documents.get(documentList.getSelectedIndex());
					int level = filterLevelBox.getSelectedIndex();
					String words = "";
					if(level > 0) {
						for (String word : getFilteredResults(level, doc)) {
							words += word + "\n";
						}
					} else {
						words = doc.filteredDoc.originalTrimDoc;
					}
					hiddenTextPane.setText(words);
//					hiddenTextPane.setText(doc.filteredDoc.docStopWords);
//					filteredResults.setText("");
					filteredResults.setViewportView(hiddenTextPane);
				}
			}
		});
		add(documentList);
		
		JLabel lblResults = new JLabel("Results");
		lblResults.setBounds(199, 117, 61, 16);
		add(lblResults);
		
		final JButton btnProcessResults = new JButton("Process Results");
		btnProcessResults.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!WekaFile.isWriting) {
					ProcessArff.showDialog();
				} else {
					JOptionPane.showMessageDialog(null, "Wait! Weka file geneartion is in progress.");
				}
			}
		});
		btnProcessResults.setBounds(220, 436, 144, 29);
		add(btnProcessResults);
		
		final JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultsFrame.setVisible(false);	
				resultsFrame.dispose();
			}
		});
		btnCancel.setBounds(387, 436, 117, 29);
		add(btnCancel);
		

	}
	
	public List<String> getFilteredResults (int level , Document doc) {
		List<String> result = new ArrayList<String>();
		switch (level) {
		case Constants.DONT_FILTER:
			break;
		case Constants.REMOVE_ONLY_STOP_WORDS:
			result = doc.filteredDoc.docStopWordList;
			break;
		case Constants.USE_ONLY_DISTINCT_WORDS:
			result = doc.filteredDoc.docDistinctWordList;
			break;
		case Constants.TOPICS:
			for (Topic topic : doc.topics) {
				result.add(topic.topicName);
			}
			break;
		}
		return result;
	}
	
	public DefaultListModel populateDocList (List<Document> documents) {
		
		for (int i=0; i<documents.size(); i++) {
			System.out.println("Doc Name : "+documents.get(i).name);
			documentListModel.add(i, documents.get(i).name+" | "+documents.get(i).clazz);
		}
//		documentListModel.ad
		return documentListModel;
	}
	
	public Dimension getPreferredSize() {
		return new Dimension(1000, 500);
	}
	
	public static void showDialog(List<Document> documents) {
		resultsFrame = new JFrame("");
		FilteredResults panel = new FilteredResults(documents);
		resultsFrame.addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowClosing(WindowEvent e) {
				// TODO Auto-generated method stub
//				System.exit(0);
			}
			
			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		resultsFrame.getContentPane().add(panel,"Center");
		
		resultsFrame.setSize(panel.getPreferredSize());
		resultsFrame.setVisible(true);
	}
	
}
