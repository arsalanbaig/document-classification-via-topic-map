
public class Type {
	public static final String TAG = "si:http://www.opencalais.com/tag";
	public static final String MAINTAG = "si:http://www.opencalais.com/classification/tag";
	public static final String TOPIC = "si:http://wandora.org/si/topic";
	public static final String RELEVANCE = "si:http://www.opencalais.com/relevance";
}
