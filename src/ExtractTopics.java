import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.plaf.SliderUI;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class ExtractTopics extends Thread {
//	private Thread thread = null;
	int apiSwitch = 0;
	static String combinationName = "";
	private File folder;
	private String location = "";
	private int filterSwitch = 0;
	private int wordFrequency = 5;
	List<Document> documents = new ArrayList<Document>();
	
	public ExtractTopics(String path, int api, int filterSwitch, int wordFrequency){
		this.location = path.split("/")[1];
		this.folder = new File(path);
		this.apiSwitch = api;
		this.filterSwitch = filterSwitch;
		this.wordFrequency = wordFrequency;
	}
	
	@Override
	public void run() {
		System.out.println("Thread # "+apiSwitch);
		Topics.getInstance();
		try {
			File[] folders = folder.listFiles();
			for(File folder : folders){
				System.out.println(folder.getAbsolutePath());
				if(folder.isDirectory()){
					File[] listoffiles = folder.listFiles();
					for(File file : listoffiles) {
						if(file.isFile()  && !file.getName().contains(".DS_Store")){
							Document document = new Document();
							document.name = file.getName();
							document.clazz = folder.getName();
							if(!Topics.classes.contains(document.clazz)){
								Topics.classes.add(document.clazz);
							}
							System.out.println("IN LOOP");
//							Thread.sleep(500);
							try{
								document.filteredDoc = StopWords.filterDistinctWords(file.getAbsolutePath(), this.wordFrequency);
								document.topics = setApiCollection(apiSwitch, document);
							} catch (Exception e){
								System.out.println(e.toString());
							}
							String flatTopics = "";
							for(Topic t : document.topics) {
								flatTopics += t.topicName + ", ";
							}
							WekaFile.writeTopicsToFile(combinationName, document.clazz, document.name, flatTopics);
//							document.tags = JCalais.retreiveTags(file.getAbsolutePath());
							document.fillTopicDict();
							documents.add(document);
						}
					}
				}
			}
			DataContainer data = new DataContainer();
			data.documents = documents;
			data.topics = Topics.getInstance();
			OutputStream documentsFile = new FileOutputStream(this.location+".dat");
			OutputStream documentsBuffer = new BufferedOutputStream(documentsFile);
	
		    ObjectOutput documentsOutput = new ObjectOutputStream(documentsBuffer);
		    {
		    	documentsOutput.writeObject(data);
		    	documentsOutput.flush();
		    	documentsOutput.close();
		    }
		}
		catch (Exception e) {
			System.out.println(e.toString());
			// TODO: handle exception
		}
//		FilteredResults filteredResultsUI = new FilteredResults();
		FilteredResults.showDialog(documents);
		Topics.generateTopicMatrixRow(documents);
		WekaFile.generateTopicFile(combinationName, documents);
		Topics.reset();
	}
	
	private List<Topic> setApiCollection(int combination, Document doc) throws IOException, ParserConfigurationException, SAXException {
		List<Topic> tempTopics;
		List<Topic> topics = new ArrayList<Topic>();
//		String text = filteredDoc.docDictinctWords;
		String text = "";
		switch(this.filterSwitch) {
		case Constants.DONT_FILTER:
			text = doc.filteredDoc.originalTrimDoc;
			break;
		case Constants.REMOVE_ONLY_STOP_WORDS:
			text = doc.filteredDoc.docStopWords;
			break;
		case Constants.USE_ONLY_DISTINCT_WORDS:
			text = doc.filteredDoc.docDictinctWords;
			break;
		default:
			text = doc.filteredDoc.docDictinctWords;
			break;
		}
		switch (combination) {
		case 0:
			topics = JCalais.retrieveTopics(text);
			if(topics.size() <= 0) {
				topics = TextAnalytics.getTopics(text);
			}
			combinationName = "JCalais-with-TextAnalytics";
			break;
		
		case 1:
			topics = Aylien.sendRequestToAylien(text);
			if(topics.size() <= 0) {
				topics = TextAnalytics.getTopics(text);
			}
			combinationName = "Aylien-with-TextAnalytics";
			break;

		case 2:
			topics = TextAnalytics.getTopics(text);
			if(topics.size() <= 0) {
				topics = Aylien.sendRequestToAylien(text);
			}
			combinationName = "TextAnalytics-with-Aylien";
			break;

		case 3:
			topics = TextAnalytics.getTopics(text);
			if(topics.size() <= 0) {
				topics = JCalais.retrieveTopics(text);
			}
			combinationName = "TextAnalytics-with-JCalais";
			break;

		case 4:
			topics = TextAnalytics.getTopics(text);
			topics.addAll(JCalais.retrieveTopics(text));
			combinationName = "TextAnalytics-and-JCalais";
			break;

		case 5:
			topics = TextAnalytics.getTopics(text);
			topics.addAll(Aylien.sendRequestToAylien(text));
			combinationName = "TextAnalytics-and-Aylien";
			break;

		case 6:
			topics = Aylien.sendRequestToAylien(text);
			if(topics.size() <= 0) {
				topics = JCalais.retrieveTopics(text);
			}
			combinationName = "Aylien-with-JCalais";
			break;

		case 7:
			topics = JCalais.retrieveTopics(text);
			if(topics.size() <= 0) {
				topics = Aylien.sendRequestToAylien(text);
			}
			combinationName = "JCalais-with-Aylien";
			break;

		case 8:
			topics = JCalais.retrieveTopics(text);
			topics.addAll(Aylien.sendRequestToAylien(text));
			combinationName = "JCalais-and-Aylien";
			break;
		case 9:
			topics = TextAnalytics.getTopics(text);
			combinationName = "TextAnalytics";
			break;
		case 10:
			topics = JCalais.retrieveTopics(text);
			combinationName = "JCalais";
			break;
		case 11:
			topics = Aylien.sendRequestToAylien(text);
			combinationName = "Aylien";
			break;
		case 12:
			topics = JCalais.retrieveTopics(text);
			tempTopics = Aylien.sendRequestToAylien(text);
			tempTopics.addAll(TextAnalytics.getTopics(text));
			for(Topic xtraTopic : tempTopics) {
				Boolean topicExist = false; 
				for(Topic t : topics) {
					if(t.topicName == xtraTopic.topicName) {
						topicExist = true;
						break;
					}
				}
				if(!topicExist) {
					topics.add(xtraTopic);
				}
			}
			combinationName = "JCalaisUAylienUTextAnalytics";
			break;
		case 13:
			tempTopics = new ArrayList<Topic>();
			List<Topic> JCalaisTopics = JCalais.retrieveTopics(text);
			List<Topic> AylienTopics = Aylien.sendRequestToAylien(text);
			List<Topic> TextATopics =TextAnalytics.getTopics(text);
			tempTopics = findCommonTopics(JCalaisTopics, AylienTopics);
			topics = findCommonTopics(tempTopics, TextATopics);
			combinationName = "JCalais|Aylien|TextAnalytics";
			break;
			
		default:
			System.out.println("Default case called in setAPICombination.");
			break;
		}
		
		return topics;
	}
	public List<Topic> findCommonTopics (List<Topic> list1, List<Topic> list2) {
		List<Topic> tempTopics = new ArrayList<Topic>();
		for(Topic jt : list1) {
			for (Topic ay : list2) {
				Boolean topicFound = false;
				String tempTopicName = ay.topicName;
				tempTopicName = tempTopicName.replaceAll(",", " ");
				tempTopicName = tempTopicName.replaceAll("-", " ");
				String[] tempTopicNames = tempTopicName.split(" ");
				for (String brokenName : tempTopicNames) {
					if(brokenName.length() > 1 && jt.topicName.contains(brokenName)) {
						tempTopics.add(ay);
						topicFound = true;
						break;
					}
				}
				if(topicFound) {
					topicFound = false;
					break;
				}
			}
		}
		return tempTopics;
	}
}
