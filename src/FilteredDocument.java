import java.util.ArrayList;
import java.util.List;


public class FilteredDocument {
	String docStopWords;
	String docDictinctWords;
	String originalTrimDoc;
	List<String> docStopWordList;
	List<String> docDistinctWordList;
	
	public FilteredDocument() {
		// TODO Auto-generated constructor stub
		docStopWords = "";
		docDictinctWords = "";
		originalTrimDoc = "";
		docStopWordList = new ArrayList<String>();
		docDistinctWordList = new ArrayList<String>();
	}
}
