import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;


@SuppressWarnings("serial")
public class Document implements Serializable {
	public String name;
	public String clazz;
	public Topic bestTopic; 
	public List<Topic> topics = new ArrayList<Topic>();
	public List<Tag> tags = new ArrayList<Tag>();
	public LinkedHashMap topicByNum = new LinkedHashMap();
	public List<Integer> topicMatrixRow = new ArrayList<Integer>();
	public List<String> words = new ArrayList<String>();
	public LinkedHashMap wordsByNum = new LinkedHashMap();
	public List<Integer> bowMatrixRow = new ArrayList<Integer>();
	public FilteredDocument filteredDoc = new FilteredDocument();
	
	public void add_topic (Topic topic) {
		topics.add(topic);
	}
	
	public void fillTopicDict () {
		for(Topic topic : topics) {
			if(Topics.topicsDict.containsKey(topic.topicName)){
				topicByNum.put(topic.topicName, Topics.topicsDict.get(topic.topicName));
			}
		}
	}
	
	public void fillBowDict() {
		for(String word : words){
			if(StopWords.totalDistinctBOW.containsKey(word)){
				wordsByNum.put(word, StopWords.totalDistinctBOW.get(word));
			}
		}
	}

}
